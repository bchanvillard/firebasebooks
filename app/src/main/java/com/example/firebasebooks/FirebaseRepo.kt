package com.example.firebasebooks

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase

class FirebaseRepo {
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val firebaseFirestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    fun getUser(): FirebaseUser?{
        return firebaseAuth.currentUser
    }

    fun getPostList(): Task<QuerySnapshot> {//Récupération des données dans Firebase
        return firebaseFirestore.collection("Livres")
            .orderBy("nom", Query.Direction.ASCENDING)
            .get()
    }

    fun savePostList(postList: List<PostModel>) {//enregistrement des données dans Firebase
        firebaseFirestore.collection("Livres").orderBy("nom", Query.Direction.ASCENDING).get().addOnCompleteListener{ task ->
            if(task.isSuccessful){
                task.result?.forEachIndexed() {i, e ->
                    firebaseFirestore.collection("Livres").document(e.id).set(postList[i])
                }
            }
        }
    }
}