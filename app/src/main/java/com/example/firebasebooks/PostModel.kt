package com.example.firebasebooks

data class PostModel (
        val nom: String = "",
        var lu: Boolean = false,
        val description: String = "",
        val couverture: String = "",
        val auteur: String = "",
        val annee: String = ""
)