package com.example.firebasebooks

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.image_details.*
import kotlinx.android.synthetic.main.image_details.view.*

class DetailsActivity : AppCompatActivity(), (PostModel) -> Unit {

    private val firebaseRepo: FirebaseRepo = FirebaseRepo()
    private var postList: List<PostModel> = ArrayList()
    private val postListAdapter: PostListAdapter = PostListAdapter(postList, this, true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(firebaseRepo.getUser() != null){
            loadPostData()
        }

        firestore_list.layoutManager = LinearLayoutManager(this)
        firestore_list.adapter = postListAdapter

        btn_return_details.visibility = View.VISIBLE
        btn_return_details.setOnClickListener {
            firebaseRepo.savePostList(postList) //Enregistrement des données au retour du menu principal
            startActivity(Intent(this@DetailsActivity, MainActivity::class.java))
            finish()
        }

        btn_logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            startActivity(Intent(this@DetailsActivity, LoginActivity::class.java))
            finish()
        }
    }

    private fun loadPostData() {
        firebaseRepo.getPostList().addOnCompleteListener{
            if(it.isSuccessful){
                postList = it.result!!.toObjects(PostModel::class.java)
                postListAdapter.postLIstItems = postList
                postListAdapter.notifyDataSetChanged()
            }else{
                Toast.makeText(
                    this@DetailsActivity,
                    it.exception!!.message.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun invoke(postModel: PostModel) {//Assignation des données pour la checkbox lu via le model
        val postItem = postList.find {
            postModel.nom == it.nom
        }
        postList[postList.indexOf(postItem)].lu = postModel.lu
    }
}