package com.example.firebasebooks

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.image_details.view.*
import kotlinx.android.synthetic.main.item_with_image.view.*


class PostListAdapter(var postLIstItems: List<PostModel>, val clickListener: (PostModel) -> Unit, val isDetailled: Boolean): RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class ImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(postModel: PostModel, clickListener: (PostModel) -> Unit){
            itemView.image_post_title.text = postModel.nom
            if(postModel.lu){
                itemView.image_post_title.setTextColor(Color.parseColor("#03a300"))
            }else{
                itemView.image_post_title.setTextColor(Color.parseColor("#ff0000"))
            }
            Glide.with(itemView.context).load(postModel.couverture).into(itemView.image_post_img)
            itemView.image_post_img.clipToOutline = true

            itemView.setOnClickListener{
                clickListener(postModel)
            }
        }
    }

    class DetailsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(postModel: PostModel, clickListener: (PostModel) -> Unit){
            itemView.details_post_title.text = postModel.nom
            itemView.details_post_annee.text = postModel.annee
            itemView.details_post_desc.text = postModel.description
            itemView.details_post_lu.isChecked = postModel.lu
            Glide.with(itemView.context).load(postModel.couverture).into(itemView.details_post_img)
            itemView.details_post_img.clipToOutline = true

            itemView.details_post_lu.setOnClickListener{
                postModel.lu = itemView.details_post_lu.isChecked
                clickListener(postModel)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(isDetailled){
            val view = LayoutInflater.from(parent.context).inflate(R.layout.image_details, parent, false)
            DetailsViewHolder(view)
        }else{
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_with_image, parent, false)
            ImageViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(isDetailled){
            (holder as DetailsViewHolder).bind(postLIstItems[position], clickListener)
        }else{
            (holder as ImageViewHolder).bind(postLIstItems[position], clickListener)
        }
    }

    override fun getItemCount(): Int {
        return postLIstItems.size
    }

}