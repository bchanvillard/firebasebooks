package com.example.firebasebooks

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.*
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_with_image.*
import kotlinx.android.synthetic.main.item_with_image.view.*

class MainActivity : AppCompatActivity(), (PostModel) -> Unit {

    private val firebaseRepo: FirebaseRepo = FirebaseRepo()
    private var postList: List<PostModel> = ArrayList()
    private val postListAdapter: PostListAdapter = PostListAdapter(postList, this, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(firebaseRepo.getUser() != null){
            loadPostData()
        }

        firestore_list.layoutManager = LinearLayoutManager(this)
        firestore_list.adapter = postListAdapter

        btn_return_details.visibility = INVISIBLE

        btn_logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
        }
    }

    private fun loadPostData() {
        firebaseRepo.getPostList().addOnCompleteListener{
            if(it.isSuccessful){
                postList = it.result!!.toObjects(PostModel::class.java)
                postListAdapter.postLIstItems = postList
                postListAdapter.notifyDataSetChanged()
            }else{
                Toast.makeText(
                    this@MainActivity,
                    it.exception!!.message.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun invoke(postModel: PostModel) {
        //Ouvrir la page de détail
        startActivity(Intent(this@MainActivity, DetailsActivity::class.java))
        finish()
    }
}